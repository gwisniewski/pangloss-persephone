import re
import sys
import os
import re
import json
import ssl
import time
import pathlib
import shutil
import datetime
import pickle

import urllib.request
import xml.etree.ElementTree as etree
from urllib.request import urlopen
from pathlib import Path

from persephone.preprocess.segmenters import segmenters

from halo import Halo


def read_xml_without_ns(filename=None):
    """
    Read a XML file stripping all namespaces
    """
    doc = etree.parse(filename)
    for el in doc.iter():

        def clean(what):
            prefix, has_namespace, postfix = what.partition("}")
            return postfix if has_namespace else what

        prefix, has_namespace, postfix = el.tag.partition('}')

        el.attrib = {clean(k): v for k, v in el.attrib.items()}
        el.tag = clean(el.tag)

    return doc.getroot()


def convert_time(what: str) -> float:
    time_reg = re.compile("PT(?P<hours>[0-9]*H)?(?P<minutes>[0-9]*M)?(?P<seconds>[0-9]+)S")

    match = time_reg.search(what)
    if match is None:
        print(what)
        import sys
        sys.exit(1)
    match = match.groupdict()

    match["hours"] = "0" if match["hours"] is None else match["hours"]
    match["minutes"] = "0" if match["minutes"] is None else match["minutes"]

    match["hours"] = match["hours"].replace("H", "")
    match["minutes"] = match["minutes"].replace("M", "")

    return int(match.get("hours", "0")) * 3600 + int(match.get("minutes", "0")) * 60 + int(match["seconds"])

    
def extract_uri_from_metadata(filename):
        
    def extract_audio(record):
        identifiers = [i.text for i in record.findall(".//identifier[@type='dcterms:URI']") if i.text.endswith("wav")]
        transcription = record.findall(".//isRequiredBy")

        assert len(identifiers) == 1

        lang_elements = [(r.attrib["code"], r.text) for r in record.findall("./metadata//subject[@code]") if r.text is not None]
        lang, language_name = zip(*lang_elements)

        language_name = [l.lower() for l in language_name]

        return {"oai": record.find("./header/identifier").text,
                "kind": "audio",
                "orig": record.find(".//extent").text,
                "duration": convert_time(record.find(".//extent").text),
                "transcription": transcription[0].text if transcription else None,
                "uri": identifiers[0].replace("http", "https"),
                # XXX better way to extract the speaker (e.g. filtering on the language)
                "speaker": sorted(r.text for r in record.findall(".//contributor[@code='speaker']")),
                "lang": set(lang), 
                "language": set(l.lower() for l in language_name)}

    def extract_trans(record):
        identifiers = [i.text for i in record.findall(".//identifier[@type='dcterms:URI']") if i.text.endswith("xml")]

        if len(identifiers) != 1:
            return {}

        lang_elements = [(r.attrib["code"], r.text) for r in record.findall("./metadata//subject[@code]") if r.text is not None]
        lang, language_name = zip(*lang_elements)
        
        return {"oai": record.find("./header/identifier").text,
                "kind": "transcription",
                "uri": identifiers[0].replace("http", "https"),
                "lang": set(lang), 
                "language": set(l.lower() for l in language_name)}
                
    extractors = {"audio/x-wav": extract_audio,
                  "text/xml": extract_trans}

    metadata = read_xml_without_ns(filename=filename)
    for record in metadata.iter("record"):
        record_format = record.findall(".//format[@type='dcterms:IMT']")

        if record.find(".//accessRights") is not None and "restricted" in record.find(".//accessRights").text.lower():
            continue
        
        if len(record_format) != 1:
            # pas de format --> collections
            continue

        assert len(record_format) == 1
        record_format = record_format[0].text

        if record_format not in extractors:
            continue

        extract = extractors[record_format]
        if extract(record):
            yield extract(record)


def download_pangloss(args):
    """
    Download resources for a given language from the pangloss collection.
    """

    def lazzy_download(output, uri):
        context = ssl._create_unverified_context()
        if not output.is_file():
            with urllib.request.urlopen(uri, context=context) as response:
                shutil.copyfileobj(response, open(ofilename, "wb"))

    wav_dir = Path(args.data_dir) / "raw_data" / "wav"
    wav_dir.mkdir(exist_ok=True, parents=True)
    
    trans_dir = Path(args.data_dir) / "raw_data" / "trans"
    trans_dir.mkdir(exist_ok=True, parents=True)

    with Halo(text=f'Downloading data for {args.language.title()}', spinner='dots'):

        audios = {}
        transcriptions = {}
        for i, record in enumerate(extract_uri_from_metadata(args.metadata)):
            assert record["language"] is not None

            if record["kind"] != "audio":
                continue
            
            if args.language.lower() not in record["language"]:
                continue

            if args.filter_speakers is not None and not args.filter_speakers.search(str(record["speaker"])):
                continue

            print(record)
            
            url = record["uri"]
            ofilename = wav_dir / os.path.basename(url)
            
            record["wav_filename"] = str(ofilename)
            audios[record["oai"]] = record

            lazzy_download(ofilename, record["uri"])

        oai_to_download = set(a["transcription"] for a in audios.values() if a["transcription"] is not None)

        for i, record in enumerate(extract_uri_from_metadata(args.metadata)):

            if record["oai"] not in oai_to_download:
                continue

            if record["kind"] != "transcription":
                continue
            
            url = record["uri"]
            ofilename = trans_dir / os.path.basename(url)

            record["transcription_fn"] = str(ofilename)
            transcriptions[record["oai"]] = record

            lazzy_download(ofilename, record["uri"])

    return transcriptions, audios


def list_languages(args):

    import pandas as pd
    
    def load_data():
        for record in extract_uri_from_metadata(args.metadata):

            if record["kind"] != "audio":
                continue
            
            yield {"lang": f"({'|'.join(sorted(record['lang']))})" if len(record["lang"]) != 1 else list(record["lang"])[0],
                   "language": f'({"|".join(sorted(record["language"]))})' if len(record["language"]) != 1 else list(record["language"])[0],
                   "has_transcription": record["transcription"] is not None,
                   "speaker": str(sorted(record["speaker"])),
                   "len": record["duration"]}

    data = pd.DataFrame(list(load_data()))

    if args.filter is not None:
        data = data[data["language"].str.contains(args.filter)]

    keys = ["lang", "language", "speaker", "has_transcription"]
    if args.without_untranscribed:
        data = data[data["has_transcription"] == True]
        data = data.drop(columns=["has_transcription"])
        keys = ["lang", "language", "speaker"]
        
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        p = data.groupby(keys).sum()
        p["len"] = p["len"].apply(lambda x: datetime.timedelta(seconds=x))
        print(p.sort_values(by=args.sort))


def print_corpus_statistics(index):
    transcriptions, audios = index

    from itertools import groupby
    from collections import defaultdict
    wav_files = groupby(sorted(audios.values(), key=lambda x: x["transcription"] is None),
                        key=lambda x: x["transcription"] is None)
    wav_files = defaultdict(list, {"untranscribed" if k else "transcribed": list(v) for k, v in wav_files})

    print(f"{len(wav_files['transcribed'])} files with a transcription ({datetime.timedelta(seconds=sum(w['duration'] for w in wav_files['transcribed']))})")
    print(f"{len(wav_files['untranscribed'])} files without a transcription ({datetime.timedelta(seconds=sum(w['duration'] for w in wav_files['untranscribed']))})")

    error = 0
    for record in audios.values():
        if record["transcription"] is not None and record["transcription"] not in transcriptions:
            error += record["duration"]
            print(f"did not find transcription with OAI {record['transcription']}")
    print(error)
    print("\n\n")


def create_dataset(args):
    """
    Code for the `create_dataset` cli argument:

    - download all required files from the pangloss collections
    - extract relevant information
    - extract features & create train/test/valid datasets
    """
    import coloredlogs
    import logging
    
    logger = logging.getLogger(__name__)
    coloredlogs.install(level='INFO')

    data_dir = args.data_dir
    index_fn = data_dir / "index.pkl"
    
    if index_fn.is_file():
        logger.info(f"Found index file {index_fn} --- assume all data have already been downloaded")
        index = pickle.load(open(index_fn, "rb"))
    else:
        index = download_pangloss(args)
        pickle.dump(index, open(index_fn, "wb"))

    if hasattr(args, "verbose") and args.verbose:
        print_corpus_statistics(index)

    from persephone.corpus import Corpus
    from functools import partial
    
    logging.info("create corpus")
    logging.info(f"using {args.segmenter_name} segmenter")
    corpus = Corpus.from_pangloss(index, args.exp_dir,
                                  segmenter=partial(segmenters[args.segmenter_name], label_type=args.label_type),
                                  label_type=args.label_type,
                                  kind_of=args.kindOf)

    return index


def train(args):

    from persephone.corpus import Corpus
    from persephone import corpus_reader

    #index = create_dataset(args, verbose=False)
    corpus = Corpus.from_pickle(args.exp_dir)
    from persephone import rnn_ctc
    data_reader = corpus_reader.CorpusReader(corpus)
    model = rnn_ctc.Model(args.exp_dir, data_reader, num_layers=args.n_layers, hidden_size=args.n_hidden)

    model.train()


def clean(args):
    import shutil
    
    to_delete = {f for f in args.exp_dir.glob("*") if f.name != 'raw_data' and not f.name.endswith('_index.json')}
    to_keep = [f for f in args.exp_dir.glob("*") if f not in to_delete]
    
    print("The following files will be deleted:", end="\n\t")
    print("\n\t".join(str(f) for f in to_delete))
    print("The following files will *not* be deleted:", end="\n\t")
    print("\n\t".join(str(f) for f in to_keep))

    if args.dry:
        return

    for f in to_delete:
        if f.is_file():
            f.unlink()
        else:
            shutil.rmtree(f)


if __name__ == "__main__":
    """
    Data organisation:

    (*) files in these directories share the same prefix (in the persephone sense)

    {exp_dir}
    │ 
    ├── {lang}_index.json  ---> OAI indexes (link between transcriptions and wav files)
    │
    ├── feat               --->                                  (*)
    │
    ├── label              ---> label e                          (*)
    │
    ├── wav                ---> wav normalized by persephone XXX (*)
    │
    └── raw_data
        │
        ├── {lang}_wav     ---> wav files downloaded from pangloss website
        │
        └── {lang}_trans   ---> transcription (pangloss xml) files downloaded from pangloss website
    """
    def parse_delta(what):
        from datetime import datetime, timedelta
        t = datetime.strptime(what,"%Hd%Mmn%Ss")
        delta = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
        return delta.total_seconds()

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--metadata", required=True)
    subparsers = parser.add_subparsers(help='sub-command help')

    list_parser = subparsers.add_parser("list", help="list all languages")
    list_parser.add_argument("--sort", choices=["lang", "len", "language"], default="language")
    list_parser.add_argument("--filter", default=None,
                             help="filter on language (argument must appear in language name")
    list_parser.add_argument("--without_untranscribed", action="store_true", default=False)
    list_parser.set_defaults(func=list_languages)
    
    create_parser = subparsers.add_parser("create_dataset", help="download data for a given language")
    create_parser.add_argument("--language", required=True)
    create_parser.add_argument("--filter_speakers", default=None, type=lambda x: re.compile(x),
                               help="if set only consider speaker matching this regexp")
    create_parser.add_argument("--kindOf", default=None)
    create_parser.add_argument("--exp_dir", required=True, type=lambda x: Path(x))
    create_parser.add_argument("--data_dir", required=True, type=lambda x: Path(x))
    create_parser.add_argument("--verbose", action="store_true")
    create_parser.add_argument("--segmenter_name", choices=segmenters.keys(), required=True)
    create_parser.add_argument("--label_type", default="phoneme", choices=["phoneme", "phoneme+tone"])
    create_parser.set_defaults(func=create_dataset)

    train_parser = subparsers.add_parser("train", help="run transcription experiment on a given language")
    train_parser.add_argument("--exp_dir", required=True, type=lambda x: Path(x))
    train_parser.add_argument("--force", action="store_true", default=False)
    train_parser.add_argument("--n_layers", type=int, default=3)
    train_parser.add_argument("--n_hidden", type=int, default=250)
    train_parser.set_defaults(func=train)

    clean_parser = subparsers.add_parser("clean", help="delete all files generated during training")
    clean_parser.add_argument("--dry", action="store_true", default=False,
                              help="only list the files to be deleted (dry-run)")
    clean_parser.add_argument("--all", action="store_true", default=False,
                              help="also delete raw data downloaded from pangloss")
    clean_parser.add_argument("--exp_dir", required=True, type=lambda x: Path(x))
    clean_parser.set_defaults(func=clean)

    args = parser.parse_args()
    args.func(args)
