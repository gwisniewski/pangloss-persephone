from pangloss_transcriber import convert_time

assert convert_time("PT0H01M36S") == 96
assert convert_time("PT0H36S") == 36
assert convert_time("PT01M36S") == 96
assert convert_time("PT0M36S") == 36
assert convert_time("PT36S") == 36
assert convert_time("PT10M35S") == 635
assert convert_time("PT1H1M1S") == 3661
assert convert_time("PT2H1M1S") == 7261
