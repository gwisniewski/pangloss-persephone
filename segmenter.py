import argparse
import unicodedata

from collections import Counter
from itertools import chain
from pathlib import Path

from persephone.preprocess.pangloss import get_sents_times_and_translations
from persephone.preprocess.segmenters import segmenters
from pprint import pprint

import coloredlogs
coloredlogs.install(level='INFO')



def pangloss_reader(data_dir, kind_of=None):
    for fn in data_dir.glob("*.xml"):
        yield from (w + (fn,) for w in zip(*get_sents_times_and_translations(fn, kind_of=kind_of)[1:-1]))

        
if __name__ == "__main__":
        
    parser = argparse.ArgumentParser()
    parser.add_argument("command", choices=["distrib", "find", "segment", "length_distribution"])
    parser.add_argument("--what")
    parser.add_argument("--kind_of", default=None)
    parser.add_argument("--data_dir", required=True, type=lambda x: Path(x))
    parser.add_argument("--segmenter", required=True, choices=segmenters.keys())

    args = parser.parse_args()
                                            
    if args.command == "distrib":
        data = (segmenters[args.segmenter](c[0]).split() for c in pangloss_reader(args.data_dir, args.kind_of))
        count = Counter(chain.from_iterable(d for d in data if d is not None))

        for key, value in sorted(count.items(), key=lambda x: -x[1]):
            print(f"{key}\t\t{[unicodedata.category(k) for k in key]}\t\t{[unicodedata.name(k) for k in key]}\t\t{value}")
        print(f"#char: {sum(count.values()):,}")
        print(f"#labels: {len(count):,}")

    if args.command == "find":
        for c in pangloss_reader(args.data_dir, args.kind_of):
            if c[0] is None:
                print(c)
                continue

            if args.what in c[0]:
                print(c)

    if args.command == "segment":

        fail = 0
        ok = 0
        for c, _, _ in pangloss_reader(args.data_dir, args.kind_of):
            r = segmenters[args.segmenter](c)
            print(c)
            print(segmenters[args.segmenter](c))
            print("-" * 10)

        #     if r is None:
        #         fail += 1
        #     else:
        #         ok += 1

        # print(f"ok = {ok}")
        # print(f"fail = {fail}")

    if args.command == "length_distribution":
        ratios = []
        for label, time, _ in pangloss_reader(args.data_dir, args.kind_of):

            tokenized_label = segmenters[args.segmenter](label)
            ratios.append(len(tokenized_label.split(" ")) / (time[1] - time[0]))

        import seaborn as sns
        plot = sns.distplot(ratios)
        fig = plot.get_figure()
        fig.savefig("pouet.pdf")
