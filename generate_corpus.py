"""Script to generate the corpus used in [1] to evaluate persephone
ability to learn a phonemic transcriber in a low-resource scenario.

Persephone has been tested on a subset of the pangloss
collection. This script regenerate this subset (or, at least, a good
approximation of it):

- it download and preprocess the wav files
- it download and preprocess the transcriptions (phonemes and tones+phonemes)
- it generate a mapping of `original' transcriptions and preprocessed transcription

"""


import xml.etree.ElementTree as etree

from pangloss_transcriber import read_xml_without_ns, extract_uri_from_metadata

file_list = ["crdo-NRU_F4_10_AGRICULTURAL_ACTIVITIES.xml", 
             "crdo-NRU_F4_35_DOG_WITH_EGG.xml",
             "crdo-NRU_F4_36_TIGER_WITH_EGG.xml",
             "crdo-NRU_F4_BURIEDALIVE2.xml",
             "crdo-NRU_F4_BURIEDALIVE3.xml",
             "crdo-NRU_F4_CARAVANS.xml",
             "crdo-NRU_F4_COMING_OF_AGE2.xml",
             "crdo-NRU_F4_DOG2.xml",
             "crdo-NRU_F4_ELDERS3.xml",
             "crdo-NRU_F4_FOOD_SHORTAGE2.xml",
             "crdo-NRU_F4_FOOD_SHORTAGE.xml",
             "crdo-NRU_F4_FUNERAL.xml",
             "crdo-NRU_F4_HEALING.xml",
             "crdo-NRU_F4_HOUSEBUILDING2.xml",
             "crdo-NRU_F4_HOUSEBUILDING.xml",
             "crdo-NRU_F4_MOUNTAINS.xml",
             "crdo-NRU_F4_MUSHROOMS.xml",
             "crdo-NRU_F4_RENAMING.xml",
             "crdo-NRU_F4_REWARD.xml",
             "crdo-NRU_F4_SEEDS2.xml",
             "crdo-NRU_F4_SEEDS.xml",
             "crdo-NRU_F4_TIGER2.xml",
             "crdo-NRU_F4_TRADER_AND_HIS_SON.xml",
             "crdo-NRU_MARIAGE_DE_LA_SOEUR_V1.xml",
             "crdo-NRU_MARIAGE_DE_LA_SOEUR_V3.xml",
             "crdo-NRU_NAISSANCE_DU_LAC_V3.xml",
             "crdo-NRU_Naissance_du_Lac_V4.xml"]

transcriptions = {}
for record in extract_uri_from_metadata("metadata_pangloss.xml"):
    if any(l in record["uri"] for l in file_list):
        transcriptions[record["oai"]] = record

audios = {}
for record in extract_uri_from_metadata("metadata_pangloss.xml"):
    if "transcription" in record and record["transcription"] in transcriptions:
        audios[record["oai"]] = record

assert len(audios) == len(file_list)        

import pickle
import os
import ssl
import urllib.request
import shutil

from pathlib import Path

# #na_f4
# context = ssl._create_unverified_context()

# trans_dir = Path("na_f4/raw_data/Na_trans")
# wav_dir = Path("na_f4/raw_data/Na_wav")

# wav_dir.mkdir(parents=True, exist_ok=True)
# trans_dir.mkdir(parents=True, exist_ok=True)

# for record in audios.values():

#     ofilename = wav_dir / os.path.basename(record["uri"])
#     print(ofilename)
#     record["wav_filename"] = str(ofilename)    
#     # with urllib.request.urlopen(record["uri"], context=context) as response:
#     #     shutil.copyfileobj(response, open(ofilename, "wb"))

# for record in transcriptions.values():
#     ofilename = trans_dir / os.path.basename(record["uri"])
#     print(ofilename)
#     record["transcription_fn"] = str(ofilename)
#     # with urllib.request.urlopen(record["uri"], context=context) as response:
#     #     shutil.copyfileobj(response, open(ofilename, "wb"))
    

# pickle.dump((transcriptions, audios), open("na_f4/raw_data/Na_index.pkl", "wb"))

from persephone.corpus import Corpus

import pickle
def original_segmenter(x, what, output_fn):

    from persephone.datasets import na
    
    with open(output_fn, "ab") as ofile:
        y = na.preprocess_na(x.text, what)
        pickle.dump({"orignal": x.text, "cleaned": y}, ofile)

        return x._replace(text=y)


# def new_segmenter(x):
#     from persephone.na import preprocess_na as new_preprocess
#     print(x.text)
#     y = new_preprocess(x.text)
#     print(y)
#     print("-" * 5)
#     # import sys
#     # sys.exit(1)
#     return x._replace(text=y)

# Corpus.from_pangloss(pickle.load(open("na_f4/raw_data/Na_index.pkl", "rb")),
#                      "na_f4/preprocessed_original_phonemes",
#                      lambda x: original_segmenter(x, "phonemes", "original_phonemes.pkl"))

Corpus.from_pangloss(pickle.load(open("na_f4/raw_data/Na_index.pkl", "rb")),
                     "na_f4/preprocessed_original_phonemes+tones",
                     lambda x: original_segmenter(x, "phonemes_and_tones", "original_phonemes+tones.pkl"))


# Corpus.from_pangloss(pickle.load(open("na_f4/raw_data/Na_index.pkl", "rb")),
#                      "na_f4/preprocessed_new_phonemes+tones",
#                      new_segmenter)

