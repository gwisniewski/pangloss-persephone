import re

from string import punctuation

from persephone.utterance import Utterance

def letter_segmenter(utterance, label_type=None, verbose=False):
    """
    """
    label = utterance.text if isinstance(utterance, Utterance) else utterance
    label = label.replace(" ", "")
    tokenized_label = " ".join(label)

    if verbose:
        print(label)
        print(tokenized_label)
        print("-" * 10)

    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label


def lowercase_without_punctuation_segmenter(utterance, label_type=None, verbose=False):
    """
    """
    
    #assert label_type is None
    
    label = utterance.text if isinstance(utterance, Utterance) else utterance
    tokenized_label = label.lower()
    tokenized_label = tokenized_label.replace(" ", "")
    tokenized_label = " ".join(l for l in tokenized_label if l not in punctuation)

    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label


def code_points(utterance, label_type=None, verbose=False):
    label = utterance.text if isinstance(utterance, Utterance) else utterance
    label = " ".join(label.split())
    import unicodedata

    #data = unicodedata.normalize("NFKC", data)
    label = re.sub("[\(\[].*?[\)\]]", "", label)
    
    tokenized_label = []
    current = ""
    for d in label:
        if d == " " or d == "\N{ZERO WIDTH NON-JOINER}" or "P" in unicodedata.category(d):
            if current:
                tokenized_label.append(current)
            current = ""
        elif d == "\N{ZERO WIDTH JOINER}":
            continue
        elif "M" in unicodedata.category(d):
            current += d
        else:
            if current:
                tokenized_label.append(current)
            current = d
    tokenized_label.append(current)
    tokenized_label = " ".join(tokenized_label)
    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label


def na_original(utterance, label_type=None, verbose=False):
    label = utterance.text if isinstance(utterance, Utterance) else utterance

    from ..datasets.na import preprocess_na

    tokenized_label = preprocess_na(label, "phonemes_and_tones")
    # XXX try to use common datatypes in order to allow factorizing
    # code at some point
    #tokenized_label = tokenized_label.split()
    
    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label


def na_orth(utterance, label_type=None, verbose=False):
    label = utterance.text if isinstance(utterance, Utterance) else utterance

    from .na_ipa2orth import ipa2orth
    label = re.sub("[\(\[].*?[\)\]]", "", label)
    if verbose:
        print(label)
    
    tokenized_label = ipa2orth(label)#.replace(" ", "")
    #tokenized_label = " ".join(t for t in tokenized_label if t not in punctuation and t not in {"…", "«", "»", "“", "”"})

    if verbose:
        print(tokenized_label)
        print("---")
        
#    if tokenized_label:
#        tokenized_label = label, tokenized_label
        
    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label

def nothing(utterance, label_type=None, verbose=False):
    label = utterance.text if isinstance(utterance, Utterance) else utterance

    tokenized_label = label.split(" ")
    
    return utterance._replace(text=tokenized_label) if isinstance(utterance, Utterance) else tokenized_label

segmenters = {"letter": letter_segmenter,
              "lowercase_without_punctuation": lowercase_without_punctuation_segmenter,
              "code_points_without_punctuations": code_points,
              "na_orth": na_orth,
              "nothing": nothing,
              "na_original": na_original,
}
