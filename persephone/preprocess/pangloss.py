""" Some functions to interface with the Pangloss """
from pathlib import Path
from typing import List
from itertools import chain

import logging

from xml.etree import ElementTree as etree
from ..utterance import Utterance

from .wav import wav_duration

logger = logging.getLogger(__name__) #type: ignore


def get_sents_times_and_translations(xml_fn, kind_of=None):
    """ Given an XML filename, loads the transcriptions, their start/end times,
    and translations. """

    tree = etree.parse(xml_fn)
    root = tree.getroot()

    if root.tag not in {"WORDLIST", "TEXT"}:
        logger.critical('the root tag, %s, does not contain "WORDLIST", and is not "TEXT"', root.tag)
        assert False, root.tag

    all_transcriptions = []
    all_times = []

    for sentence in root.findall("S"):

        audio_info = sentence.find("AUDIO")
        if audio_info is None:
            continue

        forms = sentence.findall("FORM")
        if len(forms) > 1 and kind_of is not None:
            forms = [s for s in forms if s.attrib["kindOf"] == kind_of]
            assert forms, f"kinOf={kind_of} did not select any element"
            
        if len(forms) == 0:
            elements = [s for s in sentence.findall(".//FORM") if s.text is not None]

            # XXX add a warning
            if not elements:
                continue

            if kind_of is not None:
                elements = [s for s in elements if s.attrib["kindOf"] == kind_of]
                assert elements, f"kinOf={kind_of} did not select any element"
                
            allKind = {c for c in chain.from_iterable(s.attrib.values() for s in elements)}
            if kind_of is None and len(allKind) > 1:
                raise Exception(f"several kindOf attribute found! ({allKind})\nSelect one of them with `kindOf'")

            transcription = "".join(s.text for s in elements)
        elif len(forms) == 1:
            transcription = forms[0].text
        else:
            allKind = {c for c in chain.from_iterable(f.attrib.values() for f in forms)}
            raise Exception(f"several kindOf attribute found! ({allKind})\nSelect one of them with `kindOf'")

        if transcription is None:
            logger.warning(f"empty transcription in {xml_fn}")
            continue

        start_time = float(audio_info.attrib["start"])
        end_time = float(audio_info.attrib["end"])

        all_transcriptions.append(transcription)
        all_times.append((start_time, end_time))

    return root.tag, all_transcriptions, all_times, []


def remove_content_in_brackets(sentence, brackets="[]"):
    out_sentence = ''
    skip_c = 0
    for c in sentence:
        if c == brackets[0]:
            skip_c += 1
        elif c == brackets[1] and skip_c > 0:
            skip_c -= 1
        elif skip_c == 0:
            out_sentence += c
    return out_sentence


def utterances_from_pangloss(index: Path,
                             *,
                             n_files: int = None,
                             wav_prefix: Path = Path("."),
                             kind_of: str = None) -> List[Utterance]:

    """
    Extracts utterances found in the Pangloss XML file at `pangloss_path`.
    """
    xml_idx, sound_idx = index
    assert sound_idx, "Empty index: Abort!"

    untranscribed_wav = []
    transcribed_wav = []
    n_transcriptions = 0
    n_utterances = 0
    for record_count, record in enumerate(sound_idx.values()):
        wav_fn = record["wav_filename"]

        if n_files is not None and record_count > n_files:
            logger.warning(f"breaking after reading {n_files} files")
            break

        if record["transcription"] is None:
            untranscribed_wav.append(wav_fn)
            continue

        try:
            transcription_record = xml_idx[record["transcription"]]
        except KeyError:
            logger.error(f"did not find transcription with OAI {record['transcription']}")
            continue

        n_transcriptions += 1
        pangloss_path = transcription_record["transcription_fn"]
        data = get_sents_times_and_translations(pangloss_path, kind_of=kind_of)

        doc_kind = data[0]

        transcribed_wav.append(wav_fn)
        count = 0
        for transcription, time in zip(*data[1:-1]):
            n_utterances += 1
            count += 1
            yield Utterance(org_media_path=wav_fn,
                            org_transcription_path=pangloss_path,
                            prefix="{}.{}".format(Path(wav_fn).stem, n_utterances),
                            start_time=time[0] * 1000,
                            end_time=time[1] * 1000,
                            speaker=None,
                            text=transcription,
                            doc_kind=doc_kind,
            )

    logger.debug(f"read {record_count:,} records")
    logger.debug(f"{n_transcriptions:,} transcriptions founds")
    logger.debug(f"{n_utterances:,} utterances")
    logger.debug(f"{len(transcribed_wav):,} transcribed wav files ({wav_duration(transcribed_wav)})")
    logger.debug(f"{len(untranscribed_wav):,} untranscribed wav files ({wav_duration(untranscribed_wav)})")
