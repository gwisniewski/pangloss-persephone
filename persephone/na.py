import unicodedata
import re
    
MISC_SYMBOLS = [' ̩', '~', '=', ':', 'F', '¨', '↑', '“', '”', '…', '«', '»',
                'D', 'a', 'ː', '#', '$', "‡"]
BAD_NA_SYMBOLS = ['D', 'F', '~', '…', '=', '↑', ':', "+", "˞", "\N{COMBINING DIAERESIS}"]
PUNC_SYMBOLS = [',', '!', '.', ';', '?', "'", '"', '*', ':', '«', '»', '“', '”', "ʔ"]
UNI_PHNS = {'q', 'p', 'ɭ', 'ɳ', 'h', 'ʐ', 'n', 'o', 'ɤ', 'ʝ', 'ɛ', 'g',
            'i', 'u', 'b', 'ɔ', 'ɯ', 'v', 'ɑ', 'l', 'ɖ', 'ɻ', 'ĩ', 'm',
            't', 'w', 'õ', 'ẽ', 'd', 'ɣ', 'ɕ', 'c', 'ʁ', 'ʑ', 'ʈ', 'ɲ', 'ɬ',
            's', 'ŋ', 'ə', 'e', 'æ', 'f', 'j', 'k', 'z', 'ʂ', " ̃"}
BI_PHNS = {'dʑ', 'ẽ', 'ɖʐ', 'w̃', 'æ̃', 'qʰ', 'i͂', 'tɕ', 'v̩', 'o̥', 'ts',
           'ɻ̩', 'ã', 'ə̃', 'ṽ', 'pʰ', 'tʰ', 'ɤ̃', 'ʈʰ', 'ʈʂ', 'ɑ̃', 'ɻ̃', 'kʰ',
           'ĩ', 'õ', 'dz', "ɻ̍", "wæ", "wɑ", "wɤ", "jæ", "jɤ", "jo", "ʋ̩", "ĩ"}
FILLERS = {"əəə…", "mmm…"}
# XXX le dernier phoneme est un redondant, mais les combiners ne sont pas dans le même sens
TRI_PHNS = {"tɕʰ", "ʈʂʰ", "tsʰ", "ṽ̩", "ṽ̩", "ɻ̩̃", "wæ̃", "w̃æ", "ʋ̩̃", "ɻ̩̃", "\N{LATIN SMALL LETTER V}\N{COMBINING TILDE}\N{COMBINING VERTICAL LINE BELOW}"}
UNI_TONES = {"˩", "˥", "˧"}
BI_TONES = {"˧˥", "˩˥", "˩˧", "˧˩"}
TONES = UNI_TONES.union(BI_TONES)
SYMBOLS_TO_PREDICT = {"|"}

PHONEMES = UNI_PHNS.union(BI_PHNS).union(TRI_PHNS).union(FILLERS)

BI_PHNS = {unicodedata.normalize("NFKC", c) for c in BI_PHNS}
TRI_PHNS = {unicodedata.normalize("NFKC", c) for c in TRI_PHNS}

from itertools import groupby
from operator import itemgetter

phonemes = sorted(((len(p), p) for p in PHONEMES), key=itemgetter(0))
phonemes = groupby(phonemes, key=itemgetter(0))
phonemes = {key: set(pp[1] for pp in p) for key, p in phonemes}

def preprocess_na(sent : str,
                  *,
                  normalize_extrametrical=True,
                  normalize_only=False):
    """
    Parameters:
    - normalize_extrametrical : if True, the extrametrical span symbol
      "◊" is transformed to "|". This allows to treat it as a tone group
      boundary marker for consistency with previous work.
    - normalize_only : if True, apply a `stupid' decomposition into
      phonemes (one unicode code = one phoneme)
    """
    sent = " ".join(sent.split())

    if "BEGAIEMENT" in sent:
        return ""
    
    sent = unicodedata.normalize("NFKD", sent)
    
    sent = sent.replace("wæ̃", "w̃æ")
    sent = sent.replace("ṽ̩", "ṽ̩")

    if normalize_extrametrical:
        sent = sent.replace("◊", "|")

    sent = re.sub("\[.*?\]", "", sent)

    bad_symbols = "\.|!|(?<!ə|m)…|-|,|<|>|↑|:|«|»|=|\?|;|~|ʔ|\"|‡|\(|\)|\'|\+|˞|“|”|#|¨|\*|a| ̃|F|D"
    sent = re.sub(bad_symbols, "", sent)

    sent = re.sub("ə+…", "əəə…", sent)
    sent = re.sub("m+…", "mmm…", sent)
    
    SYMBOLS = {"|", " "}

    if normalize_only:
        return " ".join(sent)
    
    orig = sent

    def pop_phoneme(sentence):

        if sentence[:4] in FILLERS:
            return sentence[:4], sentence[4:]
        if sentence[:3] in phonemes[3]:
            return sentence[:3], sentence[3:]
        if sentence[:2] in phonemes[2] | BI_TONES:
            return sentence[:2], sentence[2:]
        if sentence[0] in phonemes[1] | UNI_TONES | SYMBOLS:
            return sentence[0], sentence[1:]
        if sentence[0] == "ʰ":
            return None, sentence[1:]
        #print(f"not recognized: {sentence}")
        #print(f"already recognized: {filtered_sentence}")
        print(f"not recognized: {sentence[0]}")
        return None, sentence[1:]
        #for p in orig:
        #    print(p, unicodedata.name(p))
        
        #import sys
        #sys.exit(1)

    filtered_sentence = []
    while sent != "":
        phoneme, sent = pop_phoneme(sent)
        if phoneme != " " and phoneme is not None:
            filtered_sentence.append(phoneme)

    return " ".join(filtered_sentence)
